﻿namespace Common.Logger
{
    public interface ILogger
    {
        void Message(string message);
        void Execution(string procedure);
        void ExecutionSuccess(string procedure, bool successed);
        void Exeption(string message);
    }
}
