﻿using System;
using System.Globalization;

namespace Common.Logger
{
    public class ConsoleLogger: ILogger
    {
        private readonly DateTimeFormatInfo _dateTimeFormat;
        public ConsoleLogger(DateTimeFormatInfo dateTimeFormat)
        {
            _dateTimeFormat = dateTimeFormat;
        }

        public void Message(string message)
        {
            Console.WriteLine($@"[{Resources.MessageTypeMessage}|{DateTime.Now.ToString("f",_dateTimeFormat)}] {message}");
        }

        public void Execution(string procedure)
        {
            Console.WriteLine($@"[{Resources.MessageTypeExecution}|{DateTime.Now.ToString("f", _dateTimeFormat)}] {procedure}");
        }

        public void ExecutionSuccess(string procedure, bool successed)
        {
            var successedMessage = successed ? Resources.ExecutionSuccessedMessage : Resources.ExecutionFailedMessage;
            Console.WriteLine($@"[{Resources.MessageTypeSuccess}|{DateTime.Now.ToString("f", _dateTimeFormat)}] {procedure} {successedMessage}");
        }

        public void Exeption(string message)
        {
            Console.WriteLine($@"[{Resources.MessageTypeExeption}|{DateTime.Now.ToString("f", _dateTimeFormat)}] {message}");
        }
    }
}
