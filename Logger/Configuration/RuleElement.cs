﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Configuration
{
    public class RuleElement: ConfigurationElement
    {
        [ConfigurationProperty("pattern", IsKey = true)]
        public string FilePattern => (string) base["pattern"];

        [ConfigurationProperty("folder")]
        public string Folder => (string) base["folder"];
    }
}
