﻿using System.Configuration;
using System.Globalization;

namespace Common.Configuration
{
    public class ConfigurationsProvider: IConfigurationsProvider
    {
        public RulesConfigurationSection Rules => (RulesConfigurationSection) ConfigurationManager.GetSection("rulesSection");
        public CultureInfo Culture => new CultureInfo(ConfigurationManager.AppSettings["culture"]);
        public string ParentPath => ConfigurationManager.AppSettings["path"];

    }
}
