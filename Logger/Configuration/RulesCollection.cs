﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Configuration
{
    public class RulesCollection: ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement() => new RuleElement();

        protected override object GetElementKey(ConfigurationElement element) => ((RuleElement) element).FilePattern;
    }
}
