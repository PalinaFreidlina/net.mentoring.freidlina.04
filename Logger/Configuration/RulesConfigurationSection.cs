﻿using System.Configuration;

namespace Common.Configuration
{
    public class RulesConfigurationSection: ConfigurationSection
    {
        [ConfigurationProperty("defaultDirectory")]
        public string DefaultDirectory => (string) base["defaultDirectory"];

        [ConfigurationProperty("rules")]
        public RulesCollection Rules => (RulesCollection) this["rules"];

    }
}
