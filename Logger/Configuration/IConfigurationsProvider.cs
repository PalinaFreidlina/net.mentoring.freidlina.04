﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Configuration
{
    public interface IConfigurationsProvider
    {
        RulesConfigurationSection Rules { get; }
        CultureInfo Culture { get; }
        string ParentPath { get; }
    }
}
