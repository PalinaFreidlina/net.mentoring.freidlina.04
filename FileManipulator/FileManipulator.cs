﻿using System;
using System.IO;
using Common;
using Common.Configuration;
using Common.Logger;

namespace FileManipulator
{
    public class FileManipulator
    {
        private readonly ILogger _logger;
        private readonly IConfigurationsProvider _configProvider = new ConfigurationsProvider();
        private readonly FileSystemWatcher _watcher = new FileSystemWatcher();

        public FileManipulator()
        {
            _logger = new ConsoleLogger(_configProvider.Culture.DateTimeFormat);
            _watcher.Path = Path.GetFullPath(_configProvider.ParentPath);
            _watcher.NotifyFilter = NotifyFilters.CreationTime | NotifyFilters.FileName;
            _watcher.Created += OnCreated;
            _watcher.EnableRaisingEvents = true;
        }

        private void OnCreated(object sender, FileSystemEventArgs args)
        {
            _logger.Message($"{Resources.FileMessage} {args.Name} {Resources.Created}");
            MoveFile(args.FullPath);
        }
        
        private void MoveFile(string filePath)
        {
            _logger.Execution(Resources.MoveFile);
            try
            {
                string folder = GetFolderForNewFile(filePath);
                string destinationPath = Path.Combine(Path.GetDirectoryName(filePath), folder);
                if (!Directory.Exists(destinationPath))
                    Directory.CreateDirectory(destinationPath);
                File.Move(filePath, Path.Combine(destinationPath, Path.GetFileName(filePath)));
            }
            catch (ArgumentException e)
            {
                _logger.Exeption(e.Message);
            }
            catch (IOException e)
            {
                _logger.Exeption(e.Message);
            }
            _logger.ExecutionSuccess(Resources.MoveFile, true);
        }

        private string GetFolderForNewFile(string filePath)
        {
            _logger.Execution(Resources.GetFolderForNewFile);
            foreach (RuleElement rule in _configProvider.Rules.Rules)
            {
                if (!Path.GetFileName(filePath).Contains(rule.FilePattern)) continue;
                _logger.Message(Resources.HasRuleMessage + rule.Folder);
                return rule.Folder;
            }
            _logger.Message(Resources.HasntRuleMessage);
            return _configProvider.Rules.DefaultDirectory;
        }
    }
}
