﻿using System;
using System.Runtime.Versioning;
using System.Text;
using System.Threading;
using Common;
using Common.Configuration;
using Common.Logger;

namespace CLI
{
    class Program
    {
        private static ILogger _logger;
        private static readonly FileManipulator.FileManipulator manipulator = new FileManipulator.FileManipulator();
        private static readonly IConfigurationsProvider _configProvider = new ConfigurationsProvider();

        static void Main(string[] args)
        {
            Console.CancelKeyPress += InterruptionHandler;
            Console.OutputEncoding = Encoding.UTF8;
            Resources.Culture = _configProvider.Culture;
            _logger = new ConsoleLogger(_configProvider.Culture.DateTimeFormat);
            while (true) {}
        }

        static void InterruptionHandler(object sender, ConsoleCancelEventArgs args)
        {
            _logger.Message(Resources.InterruptedMessage);
        }
    }
}
